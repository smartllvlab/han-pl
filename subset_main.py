# coding=utf8
import argparse
import cPickle
import torch
from data_generator import DataGenerator

DIR = "./data/"
parser = argparse.ArgumentParser(description="Experiments\n")
parser.add_argument("-gpu", default=6, type=int, help="GPU number")
parser.add_argument("-batch_size", default=256, type=int, help="batch size")


args = parser.parse_args()
torch.cuda.set_device(args.gpu)


def main():
    read_f = file("./data/emb", "rb")
    embedding_matrix, _, _ = cPickle.load(read_f)
    read_f.close()
    test_generator = DataGenerator("test", args.batch_size)
    best_model = torch.load('./models/model')
    re = evaluate_test(test_generator, best_model)
    print "Performance on Test Before RL: F", re


def evaluate_test(generator, model, dataset_size=1713.0):
    pr, al1, al2, al3, zp_pres, zp_posts, nps, npc_pres, npc_posts = [], [], [], [], [], [], [], [], []
    acc_num, total_num = 0, 0
    for data in generator.generate_data():
        zp_rep, npc_rep, np_rep, feature = model.forward(data)
        output = model.generate_score(zp_rep, npc_rep, np_rep, feature)

        num = data["result"].shape[0]
        total_num += num

        output = output.data.cpu().numpy()
        for i, (s, e) in enumerate(data["s2e"]):
            if s == e:
                continue
            pr.append((data["result"][s:e], output[s:e]))
            zp_pres.append(data['zp_pre'][i])
            zp_posts.append(data['zp_post'][i])
            nps.append(data['candi'][s:e])
            npc_pres.append(data['np_pre'][s:e])
            npc_posts.append(data['np_post'][s:e])

    predict, indexs = [], []
    for result, output in pr:
        index = -1
        pro = 0.0
        for i in range(len(output)):
            if output[i] > pro:
                index = i
                pro = output[i]
        predict.append(result[index])
        indexs.append(index)
    print len(al1)
    print len(predict)

    p = sum(predict) / float(len(predict))
    r = sum(predict) / dataset_size
    f = 0.0 if (p == 0 or r == 0) else (2.0 / (1.0 / p + 1.0 / r))
    return f


if __name__ == "__main__":
    main()
